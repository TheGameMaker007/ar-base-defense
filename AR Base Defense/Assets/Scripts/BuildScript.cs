﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildScript : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] 
    public enum Buildingtype {
        Barracks,
        Tower,
        Wall,
        Gate,
        MainBase,
        GoldMine
    }
    public GameObject building;
    public GameObject marker;
    public Buildingtype buildingtype;
    public int buildCost;
    public bool canUpgrade;
    public GameObject nextBuilding;
    public int upgradeCost;
    //public Transform spawnpoint;

    public void Build()
    {
        if(PlayerPrefs.GetInt("Gold",0) >= buildCost)
        {
            PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) - buildCost);
            building.SetActive(true);
            marker.SetActive(false);
        }

    }

    public void UpgrageBuilding()
    {
        if (PlayerPrefs.GetInt("Gold", 0) >= upgradeCost && canUpgrade)
        {
            PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) - upgradeCost);
            nextBuilding.SetActive(true);
            building.SetActive(false);
        }
    }

    public void addGold()
    {
        PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + 100);
    }
}
