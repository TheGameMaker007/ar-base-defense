﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    public Transform target;

    private Transform currPos;
    private Vector3 newPos;
    // Start is called before the first frame update
    void Start()
    {
        currPos = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(currPos.position, target.position) != 0)
        {
            currPos = transform;
            Invoke("walkToTarget", 1);
        }
    }

    public void walkToTarget()
    {
        newPos = currPos.position;
        if(currPos.position.x > target.position.x)
        {
            newPos.x -= 0.0005f;
        }else if (currPos.position.x < target.position.x)
        {
            newPos.x += 0.0005f;
        }

        if(currPos.position.z > target.position.z)
        {
            newPos.z -= 0.0005f;
        }
        else if(currPos.position.z < target.position.z)
        {
            newPos.z += 0.0005f;
        }
        transform.position = newPos;
    }
}
