﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyBehavior : MonoBehaviour
{
    public Transform target;
    public GameObject waveSpawner;


    private Transform currPos;
    private Vector3 newPos;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("updateTarget", 0f, 2f);
    }

    // Update is called once per frame
    void Update()
    {
     if(target == null)
        {
            return;
        }

        if (Vector3.Distance(currPos.position, target.position) != 0)
        {
            currPos = transform;
            Invoke("walkToTarget", 1);
        }
        else
        {
            waveSpawner.GetComponent<WaveSpawner>().numBad--;
            Destroy(target.gameObject);

            if (waveSpawner.GetComponent<WaveSpawner>().numBad == 0)
            {
                waveSpawner.GetComponent<WaveSpawner>().waveComplete = true;
            }
        }
    }

    void updateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach(GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if(distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if(nearestEnemy != null)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    public void walkToTarget()
    {
        newPos = currPos.position;
        if (currPos.position.x > target.position.x)
        {
            newPos.x -= 0.0005f;
        }
        else if (currPos.position.x < target.position.x)
        {
            newPos.x += 0.0005f;
        }

        if (currPos.position.z > target.position.z)
        {
            newPos.z -= 0.0005f;
        }
        else if (currPos.position.z < target.position.z)
        {
            newPos.z += 0.0005f;
        }
        transform.position = newPos;
    }
}
