﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldCounter : MonoBehaviour
{
    public Text goldCount;
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("Gold",0) < 250)
        {
            PlayerPrefs.SetInt("Gold", 250);
        }
        goldCount.text = PlayerPrefs.GetInt("Gold", 0).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        goldCount.text = PlayerPrefs.GetInt("Gold", 0).ToString();
    }
}
