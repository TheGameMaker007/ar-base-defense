﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public Text wave;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("Wave", 1) > 1)
        {
            PlayerPrefs.SetInt("wave", 1);
        }
        wave.text = PlayerPrefs.GetInt("wave", 1).ToString();
    }

    public void GoMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    // Update is called once per frame
    void Update()
    {
        //wave.text = PlayerPrefs.GetInt("wave", 0).ToString();
    }
}
