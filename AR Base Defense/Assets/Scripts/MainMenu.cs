﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private void Start()
    {
        if(PlayerPrefs.GetInt("FirstTime", 0) == 0){
            PlayerPrefs.SetInt("FirstTime", 1); //set to true;
        }
    }


    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void GoMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ResetGame()
    {
        PlayerPrefs.SetInt("FirstTime", 0);
        PlayerPrefs.SetInt("Gold", 250);
        PlayerPrefs.SetInt("Wave", 1);
    }

    public void setSkipTutorial(bool foo)
    {
        if (foo)
        {
            PlayerPrefs.SetInt("FirstTime", 2); //set to false;
        }
        else
        {
            PlayerPrefs.SetInt("FirstTime", 1); //set to true;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
