﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScript : MonoBehaviour
{
    public bool BuildMain;
    public bool BuildBarracks;
    public bool Training;
    public bool upgrades;
    public bool otherBuildings;
    public bool endTutorial;

    private int troopCount;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("FirstTime", 0) <= 1)
        {
            endTutorial = false;
            troopCount = 0;
        }
        else
        {
            endTutorial = true;
        }
    }

    private void Update()
    {
        if (endTutorial)
        {
            gameObject.SetActive(false);
        }
        else
        {
            runTutorial();
        }
    }

    public void runTutorial()
    {
        if (BuildMain)
        {

        }
        else
        {
            if (BuildBarracks)
            {

            }
            else
            {
                if (Training)
                {
                    if(troopCount >= 5)
                    {
                        Training = false;
                    }
                }
                else
                {
                    if (upgrades)
                    {

                    }
                    else
                    {
                        if (otherBuildings)
                        {

                        }
                        else
                        {
                            if (endTutorial)
                            {
                                gameObject.SetActive(false);
                            }
                        }
                    }
                }
            }
        }
    }

    public void incTroopCount()
    {
        troopCount++;
    }


}
