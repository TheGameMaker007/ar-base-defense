﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStats : MonoBehaviour
{
    // Start is called before the first frame update
    public int Health;
    public int Armor;
    public int Attack;
}
