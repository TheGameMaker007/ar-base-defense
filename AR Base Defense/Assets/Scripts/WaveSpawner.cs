﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public Transform target;

    public Transform[] spawns;

    public GameObject[] enemies;

    public GameObject button;
    public GameObject Vscreen;
    public int numBadmax;
    public int numBad;
    public bool waveComplete;
    public bool waveDefeat;

    void spawnEnemy()
    {
        if(numBad < numBadmax)
        {
            int spawn = Random.Range(0, 13);

            GameObject t = enemies[0];
            t.GetComponent<EnemyBehavior>().target = target;
            t.transform.position = spawns[spawn].position;
            t.transform.rotation = t.transform.rotation;
            Instantiate(t);

            numBad++;
        }
        else
        {
            InvokeRepeating("checkEnd", 0f, 10f);
        }
    }

    void checkEnd()
    {
        if (waveComplete)
        {
            button.gameObject.SetActive(true);
            Vscreen.gameObject.SetActive(true);
            PlayerPrefs.SetInt("Wave", PlayerPrefs.GetInt("Wave", 1)+ 1);
            Invoke("closeWinScreen", 5f);
        }
    }

    void closeWinScreen()
    {
        Vscreen.gameObject.SetActive(false);
        waveComplete = false;
    }
    public void startWave()
    {
        InvokeRepeating("spawnEnemy", 0f, 2f);
    }
}
