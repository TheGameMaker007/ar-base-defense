﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trainTroop : MonoBehaviour
{
    public GameObject barracks;
    public GameObject unit;
    public GameObject Sword;
    public GameObject Shield;
    public int unitCost;
    public Material sword;
    public Material shield;
    public Material newSword;
    public int newSwordCost;
    public bool upgradedSword;
    public Material newArmor;
    public int newArmorCost;
    public bool upgradedArmor;
    public Transform spawnpoint;
    public GameObject upgradeButton;
    public GameObject waveSpawner;

    // Start is called before the first frame update
    private void Start()
    {
        Shield.GetComponent<Renderer>().enabled = true;
        Shield.GetComponent<Renderer>().sharedMaterial = shield;
        Sword.GetComponent<Renderer>().enabled = true;
        Sword.GetComponent<Renderer>().sharedMaterial = sword;
    }
    public void spawnTroop()
    {
        if(PlayerPrefs.GetInt("Gold", 0) >= unitCost)
        {
            PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) - unitCost);
            GameObject t = unit;
            t.transform.position = spawnpoint.transform.position;
            t.transform.rotation = unit.transform.rotation;
            t.GetComponent<FriendlyBehavior>().waveSpawner = waveSpawner;
            Instantiate(t);
        }
    }

    public void upgradeWeapons()
    {
        if (PlayerPrefs.GetInt("Gold", 0) >= newSwordCost)
        {
            PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) - newSwordCost);
            Sword.GetComponent<Renderer>().sharedMaterial = newSword;
            upgradedSword = true;
            unitCost += newSwordCost / 4;
        }

        if (upgradedArmor)
        {
            barracks.GetComponent<BuildScript>().canUpgrade = true;
            upgradeButton.SetActive(true);
        }
    }

    public void upgradeArmor()
    {
        if (PlayerPrefs.GetInt("Gold", 0) >= newArmorCost)
        {
            PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) - newArmorCost);
            Shield.GetComponent<Renderer>().sharedMaterial = newArmor;
            upgradedArmor = true;
            unitCost += newArmorCost / 4;
        }

        if (upgradedSword)
        {
            barracks.GetComponent<BuildScript>().canUpgrade = true;
            upgradeButton.SetActive(true);
        }
    }
}
